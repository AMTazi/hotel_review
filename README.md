Symfony Hotel Review
========================

Installation
------------

####1) - Clone the application from this repository:

```bash
$ git clone git@gitlab.com:AMTazi/hotel_review.git
```

Alternatively, you can use HTTPS:

```bash
$ git clone https://gitlab.com/AMTazi/hotel_review.git
```
####2) - CD to hotel_review directory

```bash
$ cd ./hotel_review
```
####3) - Create new .env file 

copy/past the content in .env.example, and change db_user, db_password and db_name
```bash
$  DATABASE_URL=mysql://root:root@127.0.0.1:3306/db_name?serverVersion=5.7
```
####4) - Install Dependencies
```bash
$ composer install
```
####4) - Create the database
```bash
$ ./bin/console doctrine:database:create
```

####5) - Migrate the database
```bash
$ ./bin/console doctrine:migrations:migrate
```

####6) - Load Fixtures
```bash
$ ./bin/console doctrine:fixtures:load
```

####7) - Run the application

```bash
$ ./bin/console server:run
```

After running the above commands, the application will run on <http://localhost:8000>:

Enjoy :D
------------