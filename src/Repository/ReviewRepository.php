<?php

namespace App\Repository;

use App\Entity\Review;
use Carbon\Carbon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Review|null find($id, $lockMode = null, $lockVersion = null)
 * @method Review|null findOneBy(array $criteria, array $orderBy = null)
 * @method Review[]    findAll()
 * @method Review[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReviewRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Review::class);
    }

    public function findByTodayDate($hotelId): ?Review
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.createdAt = :today')
            ->andWhere('r.hotel = :hotelId')
            ->orderBy('RAND()')
            ->setMaxResults(1)
            ->setParameter('hotelId', $hotelId)
            ->setParameter('today', Carbon::today()->toDateTime())
            ->getQuery()
            ->getOneOrNullResult();
    }
}
