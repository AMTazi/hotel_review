<?php

namespace App\Controller;

use App\Entity\Review;
use App\Repository\HotelRepository;
use App\Repository\ReviewRepository;
use Psr\Cache\CacheItemInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Cache\Adapter\ApcuAdapter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HotelController extends AbstractController
{
    /**
     * @Route("/", name="app_hotels")
     * @param HotelRepository $hotelRepository
     * @return Response
     */
    public function index(HotelRepository $hotelRepository)
    {
        return $this->render('hotel/index.html.twig', [
            'hotels' => $hotelRepository->findAll(),
        ]);
    }


    /**
     * @Route("/{hotelId}/today/review", name="app_hotel_today_review")
     *
     * @param int $hotelId
     * @param HotelRepository $hotelRepository
     * @param ReviewRepository $reviewRepository
     * @return Response
     */
    public function todayReview($hotelId, HotelRepository $hotelRepository, ReviewRepository $reviewRepository)
    {

        if(!$hotelRepository->find($hotelId))
            throw $this->createNotFoundException('The hotel id was not found.');

        $todayReview = $reviewRepository->findByTodayDate($hotelId);

        $this->cacheReview($todayReview);

        return $this->render('hotel/review.html.twig', [
           'todayReview' => $todayReview
        ]);

    }

    private function cacheReview(?Review $todayReview):void
    {
        if ($todayReview) {

            $reviewCacheId = 'review_'.md5((string)$todayReview->getId());

            $cache = new ApcuAdapter('', 60);
            /**
             * @var $cacheItem CacheItemInterface
             */
            $cacheItem = $cache->getItem($reviewCacheId);
            if (!$cacheItem->get()) {
                $cacheItem->set($todayReview);
                $cache->save($cacheItem);
            }
        }
    }
}
