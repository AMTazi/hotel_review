<?php

namespace App\DataFixtures;

use App\Entity\Hotel;
use App\Entity\Review;
use Carbon\Carbon;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ReviewFixture extends BaseFixture implements OrderedFixtureInterface
{
    const REVIEWS_NUMBER = 5;

    public function load(ObjectManager $manager)
    {

        $this->createMany(self::REVIEWS_NUMBER * HotelFixtures::HOTELS_NUMBER, function ($i) use ($manager){

            $hotel = $this->getHotelReference();
            $review = new Review();

            $review->setRating(rand(1, 5));
            $review->setContent($this->faker->paragraphs(3, true));
            $review->setHotel($hotel);
            $review->setCreatedAt((rand(1, 100) % 2 === 0 ? Carbon::now() : Carbon::yesterday())->toDateTime());

            $manager->persist($review);

        });

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return int
     */
    public function getOrder()
    {
        return 2;
    }

    private function getHotelReference(): Hotel
    {
        $hotelNumber = rand(1, HotelFixtures::HOTELS_NUMBER);

        /**
         * @var $hotel Hotel
         */
        $hotel = $this->getReference("hotel-" . $hotelNumber);

        return $hotel;
    }
}
