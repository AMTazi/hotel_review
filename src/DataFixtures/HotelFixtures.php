<?php

namespace App\DataFixtures;

use App\Entity\Hotel;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class HotelFixtures extends BaseFixture implements OrderedFixtureInterface
{
    const HOTELS_NUMBER = 10;

    public function load(ObjectManager $manager)
    {
        $this->createMany(self::HOTELS_NUMBER, function ($i) use ($manager){
            $hotel = new Hotel();

            $hotel->setName($this->faker->company);
            $hotel->setDescription($this->faker->paragraphs(3, true));
            $hotel->setThumbnail('https://picsum.photos/300');

            $manager->persist($hotel);
            $this->setReference("hotel-" . $i, $hotel);
        });

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return int
     */
    public function getOrder()
    {
        return 1;
    }
}
